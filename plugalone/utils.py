
from sys import stderr, argv
from os import environ, name as osname
from socket import gethostname
import argparse
from uuid import uuid4
import brain
from brain import connect
from brain.brain_pb2 import Plugin
from brain.checks import dict_to_protobuf as d2p
from brain.telemetry import get_target
from brain.controller.plugins import create_plugin
from .monitor import update_state

MY_HOSTNAME = gethostname()


def make_plugin():
    """
    Create the plugin in the brain
    based on environment variables

    :return: <dict>  rethink response dict
    """
    p2d = d2p.protobuf_to_dict
    plugin = Plugin()
    plugin.Name = environ.get("PLUGIN_NAME")
    plugin.id = environ.get("PLUGIN_ID")
    plugin.ServiceName = environ.get("SERVICE_NAME")
    plugin.ServiceID = environ.get("SERVICE_ID")
    plugin.State = "Available"
    plugin.DesiredState = ""
    plugin.OS = osname
    plugin.Interface = MY_HOSTNAME
    plugin.Extra = False
    plugin.ExternalPorts.append(environ.get("EXT_INT_PORT"))
    plugin.InternalPorts.append(environ.get("EXT_INT_PORT"))
    plugin.Environment.append("K1=V1")

    # connect to the database and register the plugin
    c = connect(host=environ.get("RETHINK_HOST"), verify=False)
    dict_plugin = p2d(plugin)
    created = create_plugin(dict_plugin, conn=c)
    if created['errors'] > 0:
        stderr.write("{} already exists\n".format(plugin.ServiceName))

    available = frozenset([""])
    response = update_state(available, "Active", "", conn=c)
    if not response:
        stderr.write("{} could not mark as Active\n".format(plugin.ServiceName))
    return response


def make_target():
    """
    create a target in the brain
    based on environment variables

    :return: <dict>  rethink response dict
    """
    c = connect(host=environ.get("RETHINK_HOST"), verify=False)
    target = {"PluginName": environ.get("PLUGIN_NAME"),
              "Port": environ.get("PORT"),
              "Location": MY_HOSTNAME,
              "Optional": {"init": ""}}
    target_exists = get_target(environ.get("PLUGIN_NAME"),
                               environ.get("PORT"),
                               MY_HOSTNAME,
                               conn=c)
    response = {"errors": 1, "first_error": "target exists"}
    if not target_exists:
        response = brain.queries.insert_target(target, conn=c)
    else:
        stderr.write("{t[PluginName]}@{t[Location]}:{t[Port]} already exists\n".format(t=target))
    return response


def remove_target():
    """
    removes a target from the brain
    based on environment variables

    :return: <dict>  rethink response dict
    """
    c = connect(host=environ.get("RETHINK_HOST"), verify=False)
    target = get_target(environ.get("PLUGIN_NAME"),
                        environ.get("PORT"),
                        MY_HOSTNAME,
                        conn=c)
    response = {}
    if target:
        response = brain.queries.RBT.get(target['id']).delete().run(c)
    return response


def destroy_plugin():
    """
    removes a plugin from the brian
    based on environment variables

    :return: <dict>  rethink response dict
    """
    conn = connect(host=environ.get("RETHINK_HOST"), verify=False)
    response = {}
    for plug in brain.controller.plugins.find_plugin(environ.get("SERVICE_NAME"),
                                                     key="ServiceName",
                                                     conn=conn):
        response = brain.controller.plugins.RPC.get(plug['id']).delete().run(conn)
        break
    return response


def notify_brain_plugin_exiting(exit_code):
    """
    updates the state of the plugin with the exit code provided

    :param exit_code: <str>  description of why the plugin is exiting
    :return: <dict>  rethink response dict
    """
    conn = connect(host=environ.get("RETHINK_HOST"), verify=False)
    any_desired_state = frozenset(["", "Stop", "Restart"])
    response = update_state(any_desired_state, exit_code, "", conn=conn)
    return response


def get_parser_bsptm():
    """
    creates an argparse.ArgumentParser with the following parameters
    -b required brain address
    -s optional stage
    -p automatically create a plugin on start (and destroy on exit)
    -t automatically ad a target on start (and remove on exit)
    -m monitor the plugin table in the brain for quit instruction

    ex:
      parser = get_parser_bsptm()

    :return: <argparse.ArgumentParser> object
    """
    parser = argparse.ArgumentParser(argv[0],
                                     description="Prepare a standalone plugin")

    parser.add_argument("-b", "--brain-address",
                        help="The IP address or DNS name of the brain (located on core manager)",
                        required=True)
    parser.add_argument("-s", "--stage",
                        help="production stage DEV|PROD",
                        required=False,
                        default="PROD")
    parser.add_argument("-p", "--add-self-as-plugin",
                        help="adds itself as a plugin (if not already exits)",
                        required=False,
                        action='store_true')
    parser.add_argument("-t", "--add-self-as-target",
                        help="adds itself as a target (if not already exits)",
                        required=False,
                        action='store_true')

    parser.add_argument("-m", "--monitor-plugin-table",
                        help="respects the desired state and shuts down if requested",
                        required=False,
                        action='store_true')

    return parser


def get_args(parser, custom_args=None):
    """
    uses a pre-definefd parser and gets the arguments

    option to specify the arguments as custom_args for testing.

    :param parser: <argparse.ArgumentParser>
    :param custom_args: <optional> <list> of command line arguments
    :return: <argparse.args>
    """
    if custom_args:
        args = parser.parse_args(custom_args)
    else:
        args = parser.parse_args()
    return args


def get_args_bsptm(custom_args=None, parser=None):
    """
    creates a parser if parser is not specified matching bsptm


    :param custom_args: <optional> <list> of arguments
    :param parser: <optional> predefined <argparse.ArgumentParser>
    :return: <argparse.args>
    """
    if not parser:
        parser = get_parser_bsptm()

    args = get_args(parser,
                    custom_args=custom_args)

    return args


def set_env_bsptm(plugin_name, args):
    """
    sets all the environment variables based on

    :param plugin_name: <str> name of the plugin to be advertised
    :param args: <argparse.args>

    :ensures sets the appropriate PCP environment variables.
    :return: None
    """
    environ['RETHINK_HOST'] = args.brain_address
    environ['PLUGIN_NAME'] = plugin_name
    environ['SERVICE_NAME'] = "{}-{}-10000tcp".format(plugin_name,
                                                      MY_HOSTNAME)
    environ['SERVICE_ID'] = str(uuid4())
    environ['PLUGIN_ID'] = str(uuid4())
    environ['EXT_INT_PORT'] = "10000/tcp"
    environ['PORT'] = "10000"
    environ["STAGE"] = args.stage
