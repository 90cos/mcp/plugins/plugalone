"""
This code expects to be run in a process

given a queue object.

if it sees an instruction to
stop or restart,
it will put the instruction into the queue
"""

from time import sleep
from os import environ
from random import randint
from brain import connect
import brain


def threaded_monitor(*args):
    """

    :param args: expects args[0] to be a multiprocess queue
    :return: None
    """
    trigger_main = frozenset(["Stop", "Restart"])
    standard_current = "Available"
    should_continue = True
    while should_continue:
        sleep(3+randint(-2, 2))
        conn = connect(host=environ.get("RETHINK_HOST"), verify=False)
        exit_code = update_state(trigger_main, standard_current, "", conn=conn)
        # stderr.write("{}\n".format(exit_code))
        if exit_code:
            args[0].put(exit_code)


@brain.queries.decorators.wrap_rethink_errors
@brain.queries.decorators.wrap_connection
def update_state(desired, current_state, future_desired, conn=None):
    """

    :param desired: <set>
    :param current_state: <str> what to change State to if successful
    :param future_desired: <str> what to change DesiredState to if successful
    :param conn: <rethink.DefaultConnection>
    :return: <str> Intended exit string or blank string
    """
    response = ""
    for plug in brain.controller.plugins.find_plugin(environ.get("SERVICE_NAME"),
                                                     key="ServiceName",
                                                     conn=conn):
        if plug['DesiredState'] in desired:
            plug['DesiredState'] = future_desired
            plug["State"] = current_state
            # stde1
            brain.controller.plugins.update_plugin(plug,
                                                   conn=conn)
            response = current_state
    return response