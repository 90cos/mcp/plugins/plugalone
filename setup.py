#!/usr/bin/python
"""Docstrings"""
from setuptools import setup, find_packages
from os import environ

# The project-level IID (internal ID) of the current pipeline. This ID is unique only within the current project.
BUILD_NUMBER = int(environ.get("CI_PIPELINE_IID", 0))

# may set SEMVER in the CI pipeline variables
SEMVER = environ.get("SEMVER", "0.3")
VERSION = f"{SEMVER}.{BUILD_NUMBER}"

setup(
    name="plugalone",
    version=VERSION,
    packages=find_packages(),
    description='RethinkDB Wrapper functions for standalone plugin development',
    author='90COS',
    maintainer='90COS',
    maintainer_email='dan@bauman.space',
    license='BSD',
    url='https://gitlab.com/90cos/mcp/plugins/plugalone',
    download_url='https://gitlab.com/90cos/mcp/plugins/plugalone',
    install_requires=[
        'ramrodbrain'
    ],
    classifiers=[
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
)
