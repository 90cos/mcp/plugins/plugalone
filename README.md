# Plugalone

A python module to assist building *standalone* python modules

## Build

see `.gitlab-ci.yml`

## install

Install from this gitlab instance pypi repo

First, ensure you have an up to date version of ramrodbrain

`pip3 install --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain`

Then, install plugalone

`pip3 install --extra-index-url https://gitlab.com/api/v4/projects/37969266/packages/pypi/simple plugalone`

